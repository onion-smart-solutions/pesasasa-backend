from django.conf.urls import  include, url
from mpesa import views

urlpatterns = [
    url(r'^stk/process/$', views.process_stk_request),
    url(r'^stk/status/$', views.check_stk_status),

]