from django.shortcuts import render
from mpesa.utils import make_request,send_notification
from datetime import datetime
from django.conf import settings
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import permissions
import base64
import json

@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def process_stk_request(request):

	phone = request.data['phone']
	amount = request.data['amount']
	reference = request.data['reference']
	desc = request.data['desc']
	timestamp = str(datetime.now())[:-7].replace('-', '').replace(' ', '').replace(':', '')
	password = base64.b64encode(bytes('{}{}{}'.format(settings.C2B_ONLINE_SHORT_CODE, settings.C2B_ONLINE_PASSKEY,timestamp), 'utf-8')).decode('utf-8')
	request={
    	"BusinessShortCode": settings.C2B_ONLINE_SHORT_CODE,
		"Password": password,
		"Timestamp": timestamp,
		"TransactionType": "CustomerPayBillOnline",
		"Amount": amount,
		"PartyA": phone,
		"PartyB": settings.C2B_ONLINE_SHORT_CODE,
		"PhoneNumber": phone,
		"CallBackURL": settings.C2B_ONLINE_CHECKOUT_CALLBACK_URL,
		"AccountReference": reference,
		"TransactionDesc": desc
		}
	response = make_request(settings.C2B_ONLINE_CHECKOUT_URL,request)
	return Response(response)

@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def check_stk_status(request):
	timestamp = str(datetime.now())[:-7].replace('-', '').replace(' ', '').replace(':', '')
	password = base64.b64encode(bytes('{}{}{}'.format(settings.C2B_ONLINE_SHORT_CODE, settings.C2B_ONLINE_PASSKEY,timestamp), 'utf-8')).decode('utf-8')
	request_id = request.data['CheckoutRequestID']
	request={
    	"BusinessShortCode": settings.C2B_ONLINE_SHORT_CODE,
		"Password": password,
		"Timestamp": timestamp,
		"CheckoutRequestID": request_id
		}
	response = make_request(settings.C2B_ONLINE_TRANSACTION_STATUS_URL,request)
	return Response(response)