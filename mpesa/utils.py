import json
import requests
from requests.auth import HTTPBasicAuth
from django.conf import settings

def get_token():
    consumer_key = settings.MPESA_C2B_ACCESS_KEY
    consumer_secret = settings.MPESA_C2B_CONSUMER_SECRET
    response = requests.get(settings.GENERATE_TOKEN_URL, auth=HTTPBasicAuth(consumer_key, consumer_secret))
    response = json.loads(response.text)
    return response['access_token']

def make_request(url,request):
	access_token = get_token()
	api_url = url
	headers = { "Authorization": "Bearer %s" % access_token }
	response = requests.post(api_url, json = request, headers=headers)
	return json.loads(response.text)

def send_notification(receiver,message):
	header = {"Content-Type": "application/json; charset=utf-8","Authorization": "Basic "+settings.ONESIGNAL_KEY}
	data = {}
	data['app_id'] = settings.ONESIGNAL_APPID
	data['include_player_ids'] = [receiver]
	data['contents'] = {"en": "Request Accepted for Processing" }
	json_data = json.dumps(data)
	response = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json_data)
	return json.loads(response.text)